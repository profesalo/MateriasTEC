gitProfesalo
La intención de este proyecto es almacenar todas las materias del Tecnológico de cada carrera ofertada, en formato moodle, con la finalidad de estandarizar la información de cada curso, permitiendo a los docentes que impartan la materia gestionar solo las actividades (ya que los recursos seran el estandar) que mejor favorezcan para el aprendizaje del Estudiante.
Todo esto siguiendo los planes de estudio actuales.
La plataforma utilizada es la de tecnológico de Arandas, mas sin embargo se hace la invitación a participar en el proyecto a los docentes de cualquier otro Tecnológico pudiendo asi servir de dicha información para favorecer el aprendizaje en sus materias.
Compartir y aportar es una forma de crecer rapidamente como individuos, pese a que el termino educación por competencias sea mal interpretado y esto genere individualización, competir juntos.

